#!/usr/bin/env python


def my_function_args(*args):
    print "my_function_args:"
    for arg in args:
        print "arg: %s" % arg
        
    print "=" * 20
        
        
my_function_args()
my_function_args("one_argument")
my_function_args("one_argument", "two arguments!")

