#!/usr/bin/env python

'''

Created on 9Jun.,2017

@author: dlawrence
'''
from random import randint


def russian_roulette():
    if randint(1, 6) == 1:
        raise ValueError("Bang!")
    return "click!"


if __name__ == '__main__':

    for i in range(10):
        try:
            russian_roulette()
        except:
            print "Lucky we didn't actually load it"
        

