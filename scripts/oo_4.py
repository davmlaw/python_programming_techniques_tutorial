#!/usr/bin/env python

'''

Created on 9Jun.,2017

@author: dlawrence
'''


class MyClass(object):
    def __init__(self, value):
        print "__init__(%s) method called" % value
        self.value = value
        

    def print_value(self):
        print "print_value(%s)" % self.value
        

if __name__ == '__main__':
    my = MyClass("Hello, world!")
    my.print_value()

    different_object = MyClass("Different object")

    different_object.print_value()
    my.print_value()
    different_object.print_value()
    my.print_value()

