#!/usr/bin/env python


def my_function_kwargs(**kwargs):
    print "my_function_kwargs(%s)" % kwargs
    
    for (key, value) in kwargs.iteritems():
        print "key: %s => %s" % (key, value)
        
    puppy = kwargs.get("puppy")
    if puppy:
        print "Puppy was passed as argument '%s'!" % puppy
    
    print "=" * 20
        
kwargs = {"always_pass" : 101}

my_function_kwargs(**kwargs)

I_WANT_MY_FURNITURE_DESTROYED = True
if I_WANT_MY_FURNITURE_DESTROYED:
    kwargs["puppy"] = "Mac"

my_function_kwargs(**kwargs)
