#!/usr/bin/env python


def my_function(value):
    print "my_function(%s)" % value
    print "=" * 20
    
# Call it    
my_function("one_argument")


try:
    my_function() # No arguments
except Exception as e:
    print e



try:
    my_function("one_argument", "two arguments!")
except Exception as e:
    print e


