#!/usr/bin/env python

'''

Created on 9Jun.,2017

@author: dlawrence
'''

class Wallet(object):
    def __init__(self, money=0):
        self.money = money
        
    def pay(self, amount):
        if amount > self.money:
            raise ValueError("Not enough money")
        else:
            self.money -= amount
            
            
    def __repr__(self):
        return "Wallet contains $%g" % self.money 
    

class Pants(object):
    def __init__(self, wallet=None):
        self.wallet = wallet

    def put_in_pocket(self, wallet):
        if self.wallet is not None:
            raise ValueError("No free space in the pocket!!")
            
        self.wallet = wallet

    def get_wallet(self):
        if self.wallet is None:
            raise ValueError("No wallet!")
        else:
            return wallet

    def __repr__(self):
        try:
            wallet = self.get_wallet()
            return "Pants containing wallet: %s" % wallet
        except:
            return "Pants contain no wallet!"



def buy_lunch_from_old_lady(pants, lunch_cost):
    print "Buying lunch for %g" % lunch_cost
    pants.wallet.pay(lunch_cost)
    

def buy_lunch_from_old_lady2(wallet, lunch_cost):
    print "Buying lunch for %g" % lunch_cost
    wallet.pay(lunch_cost)

if __name__ == '__main__':
    wallet = Wallet(50)
    pants = Pants()
    pants.put_in_pocket(wallet)

    print "Wallet: %s" % wallet
    print "Pants: %s" % pants

    buy_lunch_from_old_lady(pants, 11.50)
    print "Wallet: %s" % wallet
    print "Pants: %s" % pants
    
    buy_lunch_from_old_lady2(wallet, 9.50)
    print "Wallet: %s" % wallet
    print "Pants: %s" % pants

    # How about I store a second wallet?
    pants.put_in_pocket(Wallet(100))
    


    
