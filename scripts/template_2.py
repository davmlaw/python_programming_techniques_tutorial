#!/usr/bin/env python
import os

from Cheetah.Template import Template


if __name__ == '__main__':
    TEMPLATES_DIR = os.path.join(os.path.dirname(__file__), "..", "templates")
    
    template_file = os.path.join(TEMPLATES_DIR, "template1.tmpl")
    
    params = {"flowcell" : "170101BLAH",
              "bam" : "Sample1_bwa.hg19.bam",
              "num_occurances" : 10,
              "blah" : "blah",
              "motif" : "GATTACA",
              "rna" : "my rna"} 
    
    formatted_string = Template(file=template_file, searchList=[params])
    
    print formatted_string
    