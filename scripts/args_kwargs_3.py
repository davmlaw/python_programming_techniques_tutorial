#!/usr/bin/env python


def my_function_kwargs(**kwargs):
    print "my_function_kwargs(%s)" % kwargs

    
    for (key, value) in kwargs.iteritems():
        print "key: %s => %s" % (key, value)
        
        
    puppy = kwargs.get("puppy")
    if puppy:
        print "Puppy was passed as argument %s!" % puppy
    
    print "=" * 20
        
        
my_function_kwargs()
my_function_kwargs(hello=42)
my_function_kwargs(hello=42, puppy="Puppies are cute")

