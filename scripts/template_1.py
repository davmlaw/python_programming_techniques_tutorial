#!/usr/bin/env python

if __name__ == '__main__':

    python_template = '''flowcell: %(flowcell)s bam: %(bam)s motif: %(motif)s
num occurances: %(num_occurances)d
blah %(blah)s foo bar baz rna: %(rna)s'''
    
    
    params = {"flowcell" : "170101BLAH",
              "bam" : "Sample1_bwa.hg19.bam",
              "num_occurances" : 10,
              "blah" : "blah",
              "motif" : "GATTACA",
              "rna" : "my rna"} 
    
    print python_template % params
    print "=" * 20
    
    
    params2 = {"flowcell" : "160101BLAH",
              "bam" : "Sample2_bwa.hg19.bam",
              "num_occurances" : 15,
              "blah" : "blah blah 4321",
              "motif" : "GATTACA",
              "rna" : "RNA 2"} 
    
    print python_template % params2
    
