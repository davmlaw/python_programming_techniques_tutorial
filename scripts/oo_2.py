#!/usr/bin/env python

'''

Created on 9Jun.,2017

@author: dlawrence
'''


class MyClass(object):
    def __init__(self, value_1):
        print "__init__(%s) method called" % value_1

if __name__ == '__main__':
    my = MyClass("Hello, world!")
    
